#include "syscalls.h"

void printString_s(const char* buffer){
    asm ("int $0x80": : "a"(0), "b"(buffer):);
}

void readString_s(const char* buffer, unsigned size){
    asm ("int $0x80": : "a"(1), "b"(buffer), "c"(size):);
}

void cleanScreen_s(){
    asm ("int $0x80": : "a"(2):);
}

void listDir(unsigned int dir){
	asm ("int $0x80": : "a"(3), "b"(dir):);
}

unsigned int getFilePosition(unsigned int dir, const char* name, unsigned int flags){
	asm ("int $0x80": : "a"(4), "b"(dir), "c"(name), "d"(flags):);
}

void createDir(unsigned int dir, const char* name){
	asm ("int $0x80": : "a"(5), "b"(dir), "c"(name):);
}

void readFile(unsigned int dir){
	asm ("int $0x80": : "a"(6), "b"(dir):);
}

void createFile(unsigned int dir, const char* name){
	asm ("int $0x80": : "a"(7), "b"(dir), "c"(name):);
}

int getFileSize(unsigned int dir){
	asm ("int $0x80": : "a"(8), "b"(dir):);
}

void removeFile(unsigned int dir, const char* name){
	asm ("int $0x80": : "a"(9), "b"(dir), "c"(name):);
}