#include <system.h>
#include <screen.h>
#include <low_level.h>

void keyboard_handler(struct regs *r);
typedef void (*call_module_t)(unsigned);


typedef unsigned int multiboot_uint32_t;

struct multiboot_mod_list
{
	multiboot_uint32_t mod_start;
	multiboot_uint32_t mod_end;

	multiboot_uint32_t cmdline;

	multiboot_uint32_t pad;
};

typedef struct multiboot_mod_list multiboot_module_t;

void main(struct multiboot_info *mbinfo, uint32_t kernel_end_addr)
{
    int i;
    unsigned char buffer[512];

    gdt_init();
    idt_init();
    irq_init();
    isrs_init();

	//while (1){

	//}
    

    mm_init((mbinfo->low_mem + mbinfo->high_mem + 1024)*1024,kernel_end_addr);

    multiboot_memory_map_t *mmap_ptr = (multiboot_memory_map_t*) mbinfo->mmap_addr;
	multiboot_memory_map_t *mmap_end = (multiboot_memory_map_t*) (mbinfo->mmap_addr + mbinfo->mmap_length);

	while (mmap_ptr < mmap_end) {
	    if (mmap_ptr->type == MULTIBOOT_MEMORY_AVAILABLE) {
	        uint32_t region_base_addr = (uint32_t) (mmap_ptr->addr & 0xffffffff);
	        uint32_t region_size = (uint32_t) (mmap_ptr->len & 0xffffffff);

	        mm_mark_region_unused(region_base_addr, region_size);
	    }
	    mmap_ptr = (multiboot_memory_map_t*) ((uint32_t) mmap_ptr + mmap_ptr->size + sizeof (uint32_t));
	}

	int kernelSize = kernel_end_addr - KERNEL_BASE_ADDR;

	mm_mark_region_unused(KERNEL_BASE_ADDR, kernelSize + mm_get_bitmap_size());
	
	sti();
	irq_install_handler(1,keyboard_handler);

	init_ide_devices();

	
	

	// char buffer_r[20];
	// readString(buffer_r,20);
	// printString(buffer_r);

	mbrStart();
	

	//sti();
	if(CHECK_FLAG(mbinfo->flags,3)){
		multiboot_module_t * module_f = (multiboot_module_t *)mbinfo->mods_addr;

		call_module_t start_program = (call_module_t) module_f->mod_start;
		start_program(module_f->mod_start);
	}
	

	

	// char buffer_fat[512];
	// ide_read_blocks(0, 0, 1, &buffer_fat);
	// for(int i=0;i<40;i++){
	// 	kprintf("%c ",buffer_fat[0]);
	// }
	

	/*
	printString("hola");

	char text[40];
	readString(text,40);
	printString(text);
	*/
	while(1);
	
}