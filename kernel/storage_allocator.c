#include <system.h>

#define NALLOC 4096

static Header base = {0};
static Header *freeptr = NULL;

/* free: put block ap in free list */
void free(void *ap)
{
    Header *bp, *p;
	bp = (Header *)ap - 1;
	/* point to block header */
	for (p = freeptr; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
		if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
                    break; /* freed block at start or end of arena */
	
	if (bp + bp->s.size == p->s.ptr) {
		/* join to upper nbr */
		bp->s.size += p->s.ptr->s.size;
		bp->s.ptr = p->s.ptr->s.ptr;
	} else
		bp->s.ptr = p->s.ptr;
	
	if (p + p->s.size == bp) {
		/* join to lower nbr */
		p->s.size += bp->s.size;
		p->s.ptr = bp->s.ptr;
	} else
		p->s.ptr = bp;

	freeptr = p;
       // puts("\n Entered Free");

}

/* morecore: ask system for more memory */
Header *morecore(unsigned nu){
    char *cp = 0;
    Header *up = 0;
       
    if ((nu*sizeof(Header)) < NALLOC){
        cp = mm_alloc_frame();
        nu = 512;
    }else{
        int total_frames = ((nu*sizeof(Header)) + MM_BLOCK_SIZE-1)/MM_BLOCK_SIZE;
        cp = mm_alloc_frames(total_frames);
        nu = 512* total_frames;
    }

    kprintf("\n cp: %p", cp);   
    if (cp == (char *)0)	/* no space at all */
        return NULL;
    
    up = (Header *) cp;
    up->s.size = nu;
    free((void *)(up+1));
    return freeptr;
}

/* malloc: general-purpose storage allocator */
void *malloc(unsigned nbytes)
{
    puts("\n Entered malloc");
    Header *p, *prevptr;
    Header *morecore(unsigned);
    unsigned nunits;
    nunits = (nbytes+sizeof(Header)-1)/sizeof(Header) + 1;
    if ((prevptr = freeptr) == NULL) {
	/* no free list yet */
        puts("\n Entered Create Free List");
	base.s.ptr = freeptr = prevptr = &base;
	base.s.size = 0;
    }

    for (p = prevptr->s.ptr; ; prevptr = p, p = p->s.ptr) {
        //puts("\n Entered Search 4 Block");
        //puts("\n");
        
	if (p->s.size >= nunits) { /* big enough */
            if (p->s.size == nunits) /* exactly */
		prevptr->s.ptr = p->s.ptr;
            else {
                /* allocate tail end */
		p->s.size -= nunits;
		p += p->s.size;
		p->s.size = nunits;
            }
            freeptr = prevptr;
            return (void *)(p+1);
	}
	if (p == freeptr) /* wrapped around free list */
            if ((p = morecore(nunits)) == NULL)
                return NULL;
        
	/* none left */
    }
}
