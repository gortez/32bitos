/*===========================================================================
 *
 * types.h
 *
 * Copyright (C) 2007 - Julien Lecomte
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *===========================================================================*/

#ifndef _SIMPLIX_TYPES_H_
#define _SIMPLIX_TYPES_H_

/*===========================================================================
 *
 * Fixed width types.
 *
 * When doing any kind of low level system development, one can't use
 * the standard C data types because C data types are not the same size
 * on all architectures. The following types are defined (differently)
 * for all architectures to provide a common set of data types.
 *
 * From the "Linux Device Drivers" book (chapter 11) available at
 * http://lwn.net/Kernel/LDD3/
 *
 * arch    Size: char short int long ptr long-long u8  u16 u32 u64
 * i386            1    2    4    4   4     8       1   2   4   8
 * alpha           1    2    4    8   8     8       1   2   4   8
 * armv4l          1    2    4    4   4     8       1   2   4   8
 * ia64            1    2    4    8   8     8       1   2   4   8
 * m68k            1    2    4    4   4     8       1   2   4   8
 * mips            1    2    4    4   4     8       1   2   4   8
 * ppc             1    2    4    4   4     8       1   2   4   8
 * sparc           1    2    4    4   4     8       1   2   4   8
 * sparc64         1    2    4    4   4     8       1   2   4   8
 * x86_64          1    2    4    8   8     8       1   2   4   8
 *
 *===========================================================================*/

typedef char sint8_t;
typedef unsigned char uint8_t;

typedef short sint16_t;
typedef unsigned short uint16_t;

typedef int sint32_t;
typedef unsigned int uint32_t;

typedef long long sint64_t;
typedef unsigned long long uint64_t;


/*===========================================================================*
 * Basic types used by the kernel.                                           *
 *===========================================================================*/

typedef uint32_t addr_t;
typedef uint32_t size_t;
typedef uint32_t offset_t;
typedef uint64_t loffset_t;
typedef uint32_t time_t;
typedef sint32_t pid_t;
typedef uint8_t  ret_t;
typedef uint8_t  byte_t;

typedef enum { FALSE=0, TRUE } bool_t;

typedef struct  {
		unsigned char bitFlags;
		unsigned char firstHead;
		unsigned short firstCylinderSector;    
		unsigned char systemIdentity;
		unsigned char lastHead;
		unsigned short lastCylinderSector;
		unsigned int relativeSector;
		unsigned int sectorAmmount;
} __attribute__((packed)) masterBootRecord_partition_t;

typedef struct
{
    unsigned char offset_code[446];
    masterBootRecord_partition_t partitions[4];
    unsigned short mbrSignature;
} __attribute__((packed)) masterBootRecord_t;

typedef struct
{
	unsigned char	bootjmp[3];             
	unsigned char	oem_name[8];            
	unsigned short	bytes_per_sector;       
	unsigned char	sectors_per_cluster;   
	unsigned short	reserved_sector_count; 
	unsigned char	table_count;          
	unsigned short	root_entry_count;       
	unsigned short	total_sectors_16;      
	unsigned char	media_type;            
	unsigned short	table_size_16;         
	unsigned short	sectors_per_track;      
	unsigned short	head_side_count;        
	unsigned int	hidden_sector_count;    
	unsigned int	total_sectors_32;       
	unsigned char	bios_drive_num;        
	unsigned char	reserved1;              
	unsigned char	signature;              
	unsigned int	volume_id;              
	unsigned char	volume_label[11];       
	unsigned char	fat_type_label[8];  
	unsigned char	BootCode[448];         
	unsigned short	boot_signature;       
} __attribute__((packed)) fatPartition_t;

typedef struct {
    unsigned char	filename[8];
    unsigned char	filenameExtended[3];
    unsigned char	fileAttributes;
    unsigned char	reserved[10];
    unsigned short	creationTime;
    unsigned short	creationDate;
    unsigned short	firstCluster;
    unsigned int	size;
} __attribute__((packed)) dirTable_t;


#define NULL ((void*)0)

/* Memory Management physical address type */
typedef uint32_t physical_addr_t;

#endif /* _SIMPLIX_TYPES_H_ */
