#include "screen.h"
#include "low_level.h"
#include "system.h"

#define MAX_BUFFER 256

unsigned char kbdus[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   0,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

unsigned char internal_saved;
int currento_pos = 0;

/* Handles the keyboard interrupt */
void keyboard_handler(struct regs *r)
{
    unsigned char scancode;

    /* Read from the keyboard's data buffer */
    scancode = port_byte_in(0x60);

    /* If the top bit of the byte we read from the keyboard is
    *  set, that means that a key has just been released */
    if (scancode & 0x80)
    {
        /* You can use this one to see if the user released the
        *  shift, alt, or control keys... */
    }
    else
    {
        /* Here, a key was just pressed. Please note that if you
        *  hold a key down, you will get repeated key press
        *  interrupts. */

        /* Just to show you how this works, we simply translate
        *  the keyboard scancode into an ASCII value, and then
        *  display it to the screen. You can get creative and
        *  use some flags to see if a shift is pressed and use a
        *  different layout, or you can add another 128 entries
        *  to the above layout to correspond to 'shift' being
        *  held. If shift is held using the larger lookup table,
        *  you would add 128 to the scancode when you look for it */

        
        if(kbdus[scancode]!=0){
            if(currento_pos<MAX_BUFFER){
                internal_saved = kbdus[scancode];
                //currento_pos++;
                //cputchar_at(kbdus[scancode], 6, 6,0);
            }
        }
        //cputchar_at(kbdus[scancode], 6, 6,0);
    }
}

void cleanInternalBuffer(){
    internal_saved=0;
}

char readKey()
{
    char key = internal_saved;
    internal_saved = 0;
    
    return key;
}



void printString(char string_ar[])
{
    int i = 0;
    while(string_ar[i] != '\0'){
        kprintf("%c",string_ar[i]);
        i++;
    }
}

void readString(char * dest_buffer, int size)
{
    int current_pos=0;

    while(1){
            char key;

            cli();
            key=readKey();
            sti();

            switch(key){
                case 0:
                    continue;
                case '\b':
                    dest_buffer[current_pos]='\0';
                    current_pos--;
                    set_cursor(get_cursor()-1);
                    kprintf("%c",'\0');
                    set_cursor(get_cursor()-1);
                    break;
                case '\n':
                    dest_buffer[current_pos]='\0';
                    kprintf("%c",key);

                    return;
                default:
                    if(current_pos<MAX_BUFFER && key!=0x8 && key!=0xd){
                        dest_buffer[current_pos]=key;
                        kprintf("%c",key);
                        current_pos++;
                    }

    
                    break;
            }
    }
}


int compare_string(char *first, char *second) {
   while (*first == ((*second)-32)) {
      if (*first == '\0' || *second == '\0')
         break;
 
      first++;
      second++;
   }
 
   if (*first == '\0' && *second == '\0')
      return 0;
   else
      return -1;
}   
