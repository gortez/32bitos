#include "types.h"

#define entry_size 32

masterBootRecord_t * masterBoot_r;
fatPartition_t * fat_r;
unsigned short * GORDA;
char * buffer;
unsigned int sectorOffset;
unsigned int rootSectorOffset;
unsigned int dataSectorOffset;

void mbrStart(){
	ide_read_blocks(0,0,1,masterBoot_r);

	sectorOffset = masterBoot_r->partitions[0].relativeSector;

	ide_read_blocks(0,sectorOffset,1,fat_r);
	rootSectorOffset = fat_r->reserved_sector_count+(fat_r->table_count*fat_r->table_size_16)+sectorOffset;
	dataSectorOffset = rootSectorOffset + (fat_r->root_entry_count*entry_size/fat_r->bytes_per_sector);

//	kprintf("table_size_16: %u",fat_r->table_size_16);
//	kprintf("data_offset: %u",dataSectorOffset);
}

unsigned int computeDir(unsigned int dir){


	return (dir-2)*fat_r->sectors_per_cluster+dataSectorOffset;
}

void listDirs(unsigned int dir){
	unsigned int dirOffset = rootSectorOffset;

	if(dir!=0){
		dirOffset = computeDir(dir);
	}

	int total_dirs=fat_r->sectors_per_cluster*16;
	
	dirTable_t dir_t[total_dirs];
    ide_read_blocks(0, dirOffset, fat_r->sectors_per_cluster, dir_t);

    kprintf("Listing dirs\n");

    for(int i = 0; i < 16; i++){
    	if(dir_t[i].filename[0]==0)
    		continue;
    	
    	switch(dir_t[i].fileAttributes){
    		case 0x20:
    			kprintf("f  %s  %u  %u\n",dir_t[i].filename,dir_t[i].firstCluster,dir_t[i].size);
    			break;
    		case 0x10:
    			kprintf("d  %s  %u\n",dir_t[i].filename,dir_t[i].firstCluster);
    			break;
    	}
    }
}

unsigned int getFileCluster(unsigned int dir, char * name, unsigned int flags){
	unsigned int dirOffset = rootSectorOffset;

	if(dir!=0){
		dirOffset = computeDir(dir);
	}

	//kprintf("dirsOffset\n%u",dirOffset);

	int total_dirs=fat_r->sectors_per_cluster*16;

	dirTable_t dir_t[total_dirs];
    ide_read_blocks(0, dirOffset, fat_r->sectors_per_cluster, dir_t);

    //kprintf("changing dirs\n");

    for(int i = 0; i < 16; i++){
    	if(dir_t[i].fileAttributes==flags){
    		dir_t[i].filename[4]=0;
    		//kprintf("comparing %s = %s ?",dir_t[i].filename,name);
    		//kprintf(" dif = %d\n",compare_string(dir_t[i].filename,name));
    		if(name[0]=='.'&&name[1]=='.'&&dir_t[i].filename[0]=='.'&&dir_t[i].filename[1]=='.'){
    			return dir_t[i].firstCluster;
    		}
    		else if(compare_string(dir_t[i].filename,name)==0){
    		//	kprintf("successful!\n");
    			return dir_t[i].firstCluster;
    		}
    	}
    }
    
    kprintf("dir not found with that name!\n");
    return dir;

}

unsigned int getFileSize(unsigned int dir){
	kprintf("dir %u\n",dir);

	unsigned int address_o = sectorOffset+fat_r->reserved_sector_count;
	//kprintf("address_o %u\n",address_o);
	//unsigned short GORDA[fat_r->total_sectors_16];
	//kprintf("fat_r->table_size_16 %u\n",fat_r->table_size_16);
	ide_read_blocks(0,address_o,fat_r->table_size_16,GORDA);

	unsigned int fileSize=0;

	while(GORDA[dir]!=0xffff||GORDA[dir]!=0xfff8||GORDA[dir]!=0x0){
		fileSize +=512;
		kprintf("GORDA[%x] %x\n",dir,GORDA[dir]);
		if(GORDA[dir]==0xffff||GORDA[dir]==0xfff8)
			kprintf("size %u\n",fileSize);
			return fileSize;
		dir = GORDA[dir];
	}
}


void readFile(unsigned int dir){
	kprintf("reading file\n");
	kprintf("dir %u\n",dir);
	unsigned int address_o = sectorOffset+fat_r->reserved_sector_count;
	//kprintf("address_o %u\n",address_o);
	//unsigned short GORDA[fat_r->total_sectors_16];
	//kprintf("fat_r->table_size_16 %u\n",fat_r->table_size_16);

	unsigned int meh = computeDir(dir);
	ide_read_blocks(0,address_o,fat_r->table_size_16,GORDA);

	unsigned int filePos=0;

	//kprintf("offset computed=%u\n",computeDir(dir));
	//kprintf("GORDA[%x] %x\n",dir-1,GORDA[dir-1]);
	//kprintf("GORDA[%x] %x\n",dir,GORDA[dir]);
	//kprintf("GORDA[%x] %x\n",dir+1,GORDA[dir+1]);
	while(GORDA[dir]!=0xffff||GORDA[dir]!=0xfff8||GORDA[dir]!=0x0){
		
		//kprintf("GORDA[%x] %x\n",dir,GORDA[dir]);
		unsigned short saved = GORDA[dir];
		ide_read_blocks(0, meh, fat_r->sectors_per_cluster, buffer);
		printString(buffer);
		kprintf("\n");
		//kprintf("GORDA[%x] %x\n",dir,saved);
		filePos +=512;
		if(saved==0xffff){
			ide_read_blocks(0,sectorOffset,1,fat_r);
			return;
		}
		dir = GORDA[dir];
	}
}

unsigned short getFreeCluster(){
	unsigned int address_o = sectorOffset+fat_r->reserved_sector_count;
	unsigned int total_clusters = fat_r->table_size_16*256;

	unsigned short clusterFree = 3;

	ide_read_blocks(0,address_o,fat_r->table_size_16,GORDA);

	for(unsigned short i=3;i<total_clusters;i++){
		//kprintf("testing cluster %d\n",i);
		if(GORDA[i]==0){
			clusterFree = i;
			GORDA[i] = 0xffff;
			break;
		}
	}

	ide_write_blocks(0,address_o,fat_r->table_size_16,GORDA);
	return clusterFree;
}

void createFile(unsigned int dir, char* name, unsigned int flags){
	//unsigned int address_o = sectorOffset+fat_r->reserved_sector_count;
	//ide_read_blocks(0,address_o,fat_r->table_size_16,GORDA);
	
	unsigned short freeCluster = getFreeCluster();

	dirTable_t mine_t;
	mine_t.filename[0] = name[0]-32;
	mine_t.filename[1] = name[1]-32;
	mine_t.filename[2] = name[2]-32;
	mine_t.filename[3] = name[3]-32;
	mine_t.filename[4] = 0;

	mine_t.fileAttributes = flags;
	mine_t.firstCluster = freeCluster;
	mine_t.size = 512;

	unsigned int dirOffset = rootSectorOffset;

	ide_read_blocks(0,sectorOffset,1,fat_r);

	if(dir!=0){
		dirOffset = computeDir(dir);
	}

	unsigned int myDirOffset =  computeDir(freeCluster);
	//kprintf("freeCluster: %d\n",freeCluster);
	//kprintf("myDirOffset: %d\n",myDirOffset);

	//int total_dirs=fat_r->sectors_per_cluster*16;

	dirTable_t dir_t[64];

    ide_read_blocks(0, dirOffset, 4, dir_t);

    for(unsigned int i = 0; i < 64; i++){
    	if(dir_t[i].filename[0]==0){
    		//kprintf("found free in pos: %d\n",i);
    		//kprintf("creating: %s\n",name);

		    dir_t[i] = mine_t;
		    //kprintf("writing to file");
    		ide_write_blocks(0, dirOffset, 4, dir_t);
    		break;
    	}
    }

    if(flags==0x10){
    	dirTable_t myDir_t[64];

		dirTable_t myself;
		myself.filename[0] = '.';
		myself.filename[0] = ' ';
		myself.filenameExtended[0] = 0x0;
		myself.fileAttributes = 0x10;
		myself.firstCluster = freeCluster;
		myself.size = 0xff;

		dirTable_t parent;
		parent.filename[0] = '.';
		parent.filename[1] = '.';
		parent.filenameExtended[0] = 0x0;
		parent.fileAttributes = 0x10;
		parent.firstCluster = dir;
		parent.size = 0xff;

		myDir_t[0] = myself;
		myDir_t[1] = parent;

		//kprintf("on dir %u",myDirOffset);
		ide_write_blocks(0, myDirOffset, 4, myDir_t);
    }else if(flags==0x20){
    	char fileData[512];
    	puts("Escriba su archivo: [enter para terminar]\n");
    	readString(fileData);
    	kprintf("test if created bien\n");
    	printString(fileData);
    	ide_write_blocks(0, myDirOffset, 1, fileData);

    }

	
}

void removeFile(unsigned int dir, char* name){
	//unsigned int address_o = sectorOffset+fat_r->reserved_sector_count;
	//ide_read_blocks(0,address_o,fat_r->table_size_16,GORDA);
	
	
	unsigned int dirOffset = rootSectorOffset;

	unsigned short fileCLuster = 0; 

	//ide_read_blocks(0,sectorOffset,1,fat_r);

	if(dir!=0){
		dirOffset = computeDir(dir);
	}

	//unsigned int myDirOffset =  computeDir(freeCluster);
	//kprintf("freeCluster: %d\n",freeCluster);
	//kprintf("myDirOffset: %d\n",myDirOffset);

	//int total_dirs=fat_r->sectors_per_cluster*16;

	dirTable_t dir_t[64];

    ide_read_blocks(0, dirOffset, 4, dir_t);

    for(unsigned int i = 0; i < 64; i++){
    	if(compare_string(dir_t[i].filename,name)==0){
    		fileCLuster = dir_t[i].firstCluster;
			dir_t[i].filename[0] = 0x0;
			ide_write_blocks(0, dirOffset, 4, dir_t);
			break;
			//return dir_t[i].firstCluster;
		}
    }

    if(fileCLuster==0)
    	return;


    unsigned int address_o = sectorOffset+fat_r->reserved_sector_count;
	unsigned int total_clusters = fat_r->table_size_16*256;

	//unsigned short clusterFree = 3;

	ide_read_blocks(0,address_o,fat_r->table_size_16,GORDA);

	GORDA[fileCLuster] = 0;

	ide_write_blocks(0,address_o,fat_r->table_size_16,GORDA);

	ide_read_blocks(0,sectorOffset,1,fat_r);

	/*for(unsigned short i=3;i<total_clusters;i++){
		//kprintf("testing cluster %d\n",i);
		if(GORDA[i]==0){
			clusterFree = i;
			GORDA[i] = 0xffff;
			break;
		}
	}

	ide_write_blocks(0,address_o,fat_r->table_size_16,GORDA);*/
	

    /*
    if(flags==0x10){
    	dirTable_t myDir_t[64];

		dirTable_t myself;
		myself.filename[0] = '.';
		myself.filename[0] = ' ';
		myself.filenameExtended[0] = 0x0;
		myself.fileAttributes = 0x10;
		myself.firstCluster = freeCluster;
		myself.size = 0xff;

		dirTable_t parent;
		parent.filename[0] = '.';
		parent.filename[1] = '.';
		parent.filenameExtended[0] = 0x0;
		parent.fileAttributes = 0x10;
		parent.firstCluster = dir;
		parent.size = 0xff;

		myDir_t[0] = myself;
		myDir_t[1] = parent;

		//kprintf("on dir %u",myDirOffset);
		ide_write_blocks(0, myDirOffset, 4, myDir_t);
    }else if(flags==0x20){
    	char fileData[512];
    	puts("Escriba su archivo: [enter para terminar]\n");
    	readString(fileData);
    	kprintf("test if created bien\n");
    	printString(fileData);
    	ide_write_blocks(0, myDirOffset, 1, fileData);

    }*/

	
}



